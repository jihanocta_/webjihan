from django.apps import AppConfig


class Web4PageConfig(AppConfig):
    name = 'web4_page'
