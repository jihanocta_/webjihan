from django.apps import AppConfig


class Web2PageConfig(AppConfig):
    name = 'web2_page'
