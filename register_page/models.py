from django.db import models

# Create your models here.
class Jadwalkegiatan(models.Model):
    activity = models.CharField(max_length=30)
    date = models.DateField()
    time = models.TimeField()
    location = models.CharField(max_length=50)
    category = models.CharField(max_length=20)