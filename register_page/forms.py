from django import forms
class Jadwal_kegiatan(forms.Form):

    activity = forms.CharField(label = 'Activity', required=True, max_length=27, widget=forms.TextInput)
    date = forms.DateField(label = 'Date', required=True, widget=forms.DateInput(attrs={'type':'date'}))
    time = forms.TimeField(label = 'Time', required=True, widget=forms.TimeInput(attrs={'type':'time'}))
    location = forms.CharField(label = 'Location', required=True, max_length=27, widget=forms.TextInput)
    category = forms.CharField(label = 'Category', required=True, max_length=27, widget=forms.TextInput)