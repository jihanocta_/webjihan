from django.shortcuts import render
from .forms import Jadwal_kegiatan
from .models import Jadwalkegiatan

from django.http import HttpResponseRedirect

# Create your views here.
def register(request):
    if request.method == 'POST':
        form_jadwal = Jadwal_kegiatan(request.POST)
        print(form_jadwal)
        print(form_jadwal.errors)
        print(form_jadwal.is_valid())
        if form_jadwal.is_valid():
            data = form_jadwal.cleaned_data
            Jadwalkegiatan.objects.create(**data)
            return HttpResponseRedirect('/activityview')
    else:
        form_jadwal = Jadwal_kegiatan()
    content = {'form' : form_jadwal}
    return render(request, 'register_page/register.html', content)

def activity_view(request):
    if request.method == 'POST':
        print('masuk sini')
        Jadwalkegiatan.objects.all().delete()
        return HttpResponseRedirect('/activityview')
    content = {'activity': list(Jadwalkegiatan.objects.all())}
    return render(request, 'register_page/activityview.html', content)

