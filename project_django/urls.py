"""project_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import re_path
from home_page.views import home
from olim_page.views import olim
from compfest_page.views import compfest
from register_page.views import register
from ristek_page.views import ristek
from register_page.views import activity_view

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path('^$', home, name="home"),
    re_path('^olim', olim, name="olim"),
    re_path('^compfest', compfest, name="compfest"),
    re_path('^ristek', ristek, name="ristek"),
    re_path('^register', register, name="register"),
    re_path('^activityview', activity_view, name="activityview")
]
    