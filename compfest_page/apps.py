from django.apps import AppConfig


class Web3PageConfig(AppConfig):
    name = 'web3_page'
